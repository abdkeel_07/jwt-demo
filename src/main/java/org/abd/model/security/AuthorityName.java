package org.abd.model.security;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}
